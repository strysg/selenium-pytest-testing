DEFAULT_PROJECT=openlibrary
PROJECT=${DEFAULT_PROJECT}

setup:
	virtualenv --python=python3 .venv
	. .venv/bin/activate
	python3 -m pip install pipenv
	pipenv install -d
	@echo "Setup Done!"

static_code_analysis:
	@echo "Static Code analysis"
	pipenv run pylint main/
	pipenv run pylint tests/
	pipenv run flake8 main/ --benchmark
	pipenv run flake8 tests/ --benchmark
	pipenv run pycodestyle main/ --benchmark
	pipenv run pycodestyle tests/ --benchmark

test:
	@echo "Executing tests ${PROJECT}"
	pipenv run pytest --cache-clear tests/${PROJECT}