from selenium.webdriver.common.by import By
from main.core.utils.custom_logger import CustomLogger


class HeaderBar:
    """Header Bar Element of the openlibrary web page
    """
    LOGGER = CustomLogger(__name__)
    header_container = (By.ID, "header-bar")
    hamburger_dropdown = (
        By.CSS_SELECTOR, 
        "#header-bar > div.hamburger-component.header-dropdown > details > div"
    )
    navigation_component = (By.XPATH, "//ul[@class='navigation-component']")
    browse_dropdown = (By.XPATH, "//div[@class='browse-component header-dropdown']")
    browse_subjects = (By.XPATH, "//div[@class='v']/ul/li/a[@href='/subjects']")
    browse_collections = (By.XPATH, "//div[@class='v']/ul/li/a[@href='/collections']")
    browse_k12 = (
        By.XPATH, 
        "//div[@class='v']/ul/li/a[@href='/k-12']"
    )
    browse_random = (
        By.XPATH, 
        "//div[@class='v']/ul/li/a[@href='/random']"
    )
    advanced_search = (
        By.XPATH, 
        "//div[@class='v']/ul/li/a[@href='/advancedsearch']"
    )
    more_dropdown = (
        By.XPATH, 
        "//div[@class='more-component header-dropdown']"
    )
    more_add_book = (
        By.XPATH, 
        "//ul[@class='dropdown-menu more-dropdown-menu']/li/a[@href='/books/add']"
    )
    more_sponsor = (
        By.XPATH, 
        "//ul[@class='dropdown-menu more-dropdown-menu']/li/a[@href='/sponsorship']"
    )
    more_recent = (
        By.XPATH, 
        "//ul[@class='dropdown-menu more-dropdown-menu']/li/a[@href='/recentchanges']"
    )
    more_developers = (
        By.XPATH, 
        "//ul[@class='dropdown-menu more-dropdown-menu']/li/a[@href='/developers']"
    )
    more_help = (
        By.XPATH, 
        "//ul[@class='dropdown-menu more-dropdown-menu']/li/a[@href='/help']"
    )
    search_filter_by = (By.XPATH, "//select[@aria-label='Search by']")
    search_filter_title = (
        By.XPATH, 
        "//select[@aria-label='Search by']/option[@value='all']"
    )
    search_filter_author = (
        By.XPATH, 
        "//select[@aria-label='Search by']/option[@value='author']"
    )
    search_filter_text = (
        By.XPATH, 
        "//select[@aria-label='Search by']/option[@value='text']"
    )
    search_filter_subject = (
        By.XPATH, 
        "//select[@aria-label='Search by']/option[@value='subject']"
    )
    search_filter_lists = (
        By.XPATH, 
        "//select[@aria-label='Search by']/option[@value='lists']"
    )
    search_filter_advanced = (
        By.XPATH, 
        "//select[@aria-label='Search by']/option[@value='advanced']"
    )
    search_input = (By.XPATH, "//input[@name='q']")
    search_submit = (By.XPATH, "//input[@type='submit' and @class='search-bar-submit']")
    profile_dropdown = (By.CSS_SELECTOR, ".account-component .header-dropdown__icon")
    login_page_button = (By.XPATH, "//ul[@class='auth-component']/li/a[@href='/account/login']")
    login_button = (By.XPATH, "//input[@type='submit'][@name='login']")
    logout_button = (By.XPATH, "//form[@action='/account/logout']/button")

    def __init__(self):
        pass
