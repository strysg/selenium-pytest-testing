from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from main.core.utils.custom_logger import CustomLogger

class TopNotice:
    """Top Notice Element of the openlibrary web page
    """
    LOGGER = CustomLogger(__name__)
    top_notice_div = (By.ID, "topNotice")
    internet_archive_logo = (By.CSS_SELECTOR, "//img[@alt='Internet Archive logo']")
    bar_message = (By.XPATH, "//div[@id='topNotice']//div[@class='iaBarMessage']")
    donate_button = (By.XPATH, "//div[@class='iaBarMessage']//a[@class='ghost-btn']")

    def __init__(self):
        pass
        
    # TODO: Implement
    def donate(self):
        pass