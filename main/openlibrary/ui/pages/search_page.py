from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from main.core.utils.custom_logger import CustomLogger
from main.openlibrary.ui.components.top_notice import TopNotice
from main.openlibrary.ui.components.header_bar import HeaderBar

class SearchPage():
    """Main page for the openlibrary website
    """
    LOGGER = CustomLogger(__name__)
    navigation_bar = (By.CSS_SELECTOR, "#header-bar > ul")
    search_bar = (By.CSS_SELECTOR, "#header-bar > div.search-component")
    search_input = (By.ID, "q")
    search_submit = (By.ID, "searchsubmit")

    def __init__(self, driver, configs):
        self.top_notice = TopNotice()
        self.header_bar = HeaderBar()
        self.driver = driver
        self.configs = configs
        self.w = WebDriverWait(driver, 7)
        # goto base path
        self.driver.get(configs["url"])

    def search_term(self, term="old man"):
        self.driver.find_element(*self.header_bar.search_input).send_keys(term)
        # TODO: add explicit wait
        self.driver.find_element(*self.header_bar.search_submit).click()
