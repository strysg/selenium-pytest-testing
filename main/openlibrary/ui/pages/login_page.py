from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from main.core.utils.custom_logger import CustomLogger
from main.openlibrary.ui.components.top_notice import TopNotice
from main.openlibrary.ui.components.header_bar import HeaderBar


class LoginPage():
    """Login page for the openlibrary website
    """
    LOGGER = CustomLogger(__name__)
    input_username = (By.XPATH, "//input[@id='username']")
    input_password = (By.XPATH, "//input[@id='password']")

    def __init__(self, driver, configs) -> None:
        self.top_notice = TopNotice()
        self.header_bar = HeaderBar()
        self.driver = driver
        self.configs = configs
        self.w = WebDriverWait(driver, 7)
        self.driver.get(configs["url"])

    def login(self, username, password):
        self.driver.find_element(*self.header_bar.login_page_button).click()
        # TODO: add explicit login
        self.driver.find_element(*self.input_username).send_keys(username)
        self.driver.find_element(*self.input_password).send_keys(password)
        self.driver.find_element(*self.header_bar.login_button).click()

    # TODO: Simulate user actions, don't go to endpoint
    def logout(self):
        self.driver.find_element(*self.header_bar.profile_dropdown).click()
        self.driver.find_element(*self.header_bar.logout_button).click()
