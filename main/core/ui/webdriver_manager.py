"""Web driver manager.
"""
from selenium import webdriver
from main.core.utils.configs import Configs


class WebDriverManager:
    def __init__(self, browser="chrome", **kwargs):
        """Initializes a selenium web driver given the arguments.

        Args:
            browser (str, optional): Browser to use. Defaults to "chrome".
            implicit_wait (int, optional): seconds to the default implicit wait.
                By default it does not use "implicitly_wait"
        """
        driver_path = Configs.get_instance().config_json['drivers'][browser]
        self.driver = None
        if browser == "chrome":
            self.driver = webdriver.Chrome(executable_path=driver_path)
        elif browser == 'firefox':
            self.driver = webdriver.Firefox(executable_path=driver_path)
        elif browser == 'edge':
            self.driver = webdriver.Edge(executable_path=driver_path)

        for arg_name, value in kwargs.items():
            if arg_name == 'implicit_wait':
                self.driver.implicitly_wait(value)
            
        self.driver.maximize_window()

    def close(self):
        """Closes the web driver object
        """
        self.driver.close()