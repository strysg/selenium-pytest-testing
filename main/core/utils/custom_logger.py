"""Module to manage loggers
"""
import logging


class CustomLogger(logging.Logger):
    """Class to manage a custom logger

    Parameters
    ----------
    logging : obj
        logging object
    """
    handlers = []

    def __init__(self, name, logfilepath="logs.log"):
        """Initialize logger

        Parameters
        ----------
        name : context
            context where logger runs

        logfilepath(string) : Path for the logfile default logs.log
        """
        logging.Logger.__init__(self, name)
        self.configure(logfilepath)

    def close(self):
        """Method to close and flush all the handlers
        """
        for handler in self.handlers:
            handler.flush()
            handler.close()

    def configure(self, logfilepath):
        """Method to configure handlers, set level and set formatters.
        """
        console_handler = logging.StreamHandler()
        file_handler = logging.FileHandler(logfilepath)
        console_handler.setLevel(logging.DEBUG)
        file_handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
            datefmt="%d-%b-%y %H:%M:%S"
            )
        console_handler.setFormatter(formatter)
        file_handler.setFormatter(formatter)
        self.handlers = self.handlers + [console_handler, file_handler]
