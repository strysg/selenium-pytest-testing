import os
from main.core.utils.jsonreader import JsonReader


class Configs:
    """Configs loader
    """

    __instance = None

    def __init__(self, 
            config_json="main/core/resources/config.json",
            environment_json="main/core/resources/environment.json"
        ):
        """Loads the config files "config.json" and "environment.json".

        Parameters
        ----------
        config_json: str, optional
            config.json file to read, default "/main/core/resources/config.json"

        environment_json: str, optional
            environment.json file to read, default "/main/core/resources/environment.json"
        """

        config_json = os.path.join(os.path.sep, os.curdir, "main", "core", "resources", "config.json")
        environment_json = os.path.join(os.path.sep, os.curdir, "main", "core", "resources", "environment.json")

        self.__config_json = JsonReader().get_json(config_json)
        self.__environment_json = JsonReader.get_json(environment_json)

        self.config_json = self.__config_json
        self.environment_json = self.__environment_json

    @staticmethod
    def get_instance():
        """This method get an isntance of the Configs class.
        """
        if Configs.__instance is None:
            Configs.__instance = Configs()
        return Configs.__instance

    @staticmethod
    def get_configs(site="orangehrmlive", environment="DEVELOPMENT"):
        """Returns the dict of configs for the given site

        Args:
            site (str, optional): Site name. Defaults to "orangehrmlive".
            environment (str, optional): Environment to use, Default to DEVELOPMENT

        Returns:
            Dict or None if not exists
        """
        return Configs.get_instance().environment_json.get(environment, {}).get(site, None)