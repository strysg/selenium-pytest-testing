class HomePage:
    def __init__(self, driver, configs) -> None:
        self.driver = driver
        self.configs = configs
        self.driver.get(configs["url"])

        self.l_init_menu = '//ul[@id="menu-topmenu"]/li/a[@title="Harry Potter – BlogHogwarts"]'
        self.l_subtitle = '//div[@class="site-tagline"]/span[0]'
        self.l_search_input = '//aside[@id="search-4"]/form/div/input[@name="s"]'
        self.l_search_button = '//aside[@id="search-4"]/form/div/button[@type="submit"]'

    def load_init(self):
        self.driver.get(self.configs["url"])
        
        self.title = self.driver.title
        self.init_menu = self.driver.find_element_by_xpath(self.l_init_menu)
        self.subtitle = self.driver.find_element_by_xpath(self.l_subtitle)
        self.search_input = self.driver.find_element_by_spath(self.l_search_input)
        self.search_button = self.driver.find_element_by_spath(self.l_search_button)
        
    def search_term(self, term="Snape"):
        self.search_input.value = term
        self.search_button.click()