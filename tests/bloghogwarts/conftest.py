import pytest
from selenium import webdriver
from main.core.utils.configs import Configs
from main.core.utils.custom_logger import CustomLogger

LOGGER = CustomLogger(__name__)

@pytest.fixture(scope="class")
def driver():
    LOGGER.debug(f"Loading driver....")
    chrome_driver_path = Configs.get_instance().config_json['drivers']['chrome']
    driver = webdriver.Chrome(executable_path=chrome_driver_path)
    driver.implicitly_wait(10)
    driver.maximize_window()    

    yield driver
    driver.close()
    LOGGER.debug("test_setup completed")

@pytest.fixture(scope="class")
def site_configs():
    return Configs.get_configs(site="bloghogwarts")