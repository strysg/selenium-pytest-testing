import pytest
from assertpy import assert_that
from main.core.utils.configs import Configs
from main.bloghogwarts.home_page import HomePage
from main.core.utils.custom_logger import CustomLogger

LOGGER = CustomLogger(__name__)

def test_homepage(driver, site_configs):
    LOGGER.info(f"TEST HOEMA PAGE {site_configs}")
    home_page = HomePage(driver, site_configs)
    home_page.load_init()
    assert_that(home_page.title).is_equal_to("Harry Potter en BlogHogwarts.com | Animales Fantásticos")
    assert_that(home_page.init_menu.value).is_equal_to("Harry Potter – BlogHogwarts")
