import pytest
from selenium import webdriver
from main.core.utils.configs import Configs
from main.core.utils.custom_logger import CustomLogger

LOGGER = CustomLogger(__name__)

@pytest.fixture(scope="class")
def driver():
    chrome_driver_path = Configs.get_instance().config_json['drivers']['chrome']
    driver = webdriver.Chrome(executable_path=chrome_driver_path)
    driver.implicitly_wait(10)
    driver.maximize_window()    

    yield driver
    driver.close()
    LOGGER.debug("test_setup completed")

@pytest.fixture
def site_configs():
    LOGGER.debug(f"Configs: {Configs.get_configs()}")
    return Configs.get_configs(site="orangehrm")

@pytest.fixture
def login(driver, site_configs):
    endpoint = f"{site_configs['url']}{site_configs['endpoints']['login']}"
    driver.get(endpoint)
    driver.find_element_by_id("txtUsername").send_keys(site_configs['username'])
    driver.find_element_by_id("txtUsername").send_keys(site_configs['password'])
    driver.find_element_by_id("btnLogin").click()
    assert driver.title == "OrangeHRM"
