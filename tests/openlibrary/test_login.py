import pytest
from assertpy import assert_that
from main.core.utils.custom_logger import CustomLogger
from main.openlibrary.ui.pages.login_page import LoginPage


LOGGER = CustomLogger(__name__)

@pytest.fixture(scope="class")
def test_login_page(driver, site_configs):
    LOGGER.info("Doing login.")
    page = LoginPage(driver, site_configs)
    page.login(site_configs["username"], site_configs["password"])
    assert_that(page.driver.title).is_equal_to("Welcome to Open Library | Open Library")
    yield page
    LOGGER.info("Finished login.")

def test_login(test_login_page):
    LOGGER.info("Login in")

def test_logout(test_login_page):
    LOGGER.info("Do logout...")
    page = test_login_page
    page.logout()

    init_session_button = page.driver.find_element(*page.header_bar.login_page_button)
    LOGGER.debug(f"initi session: {init_session_button.text}")
    assert_that(init_session_button.text).is_equal_to("Log In")