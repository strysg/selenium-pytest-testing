import pytest
from main.core.ui.webdriver_manager import WebDriverManager
from main.core.utils.configs import Configs
from main.core.utils.custom_logger import CustomLogger

LOGGER = CustomLogger(__name__)

@pytest.fixture(scope="class")
def driver():
    LOGGER.debug(f"Loading driver....")
    web_driver = WebDriverManager("chrome", implicit_wait=10)

    yield web_driver.driver
    web_driver.close()
    LOGGER.debug("test_setup completed")

@pytest.fixture(scope="class")
def site_configs():
    return Configs.get_configs(site="openlibrary")