import pytest
from assertpy import assert_that
from selenium import webdriver
from main.core.utils.custom_logger import CustomLogger
from main.openlibrary.ui.pages.search_page import SearchPage


LOGGER = CustomLogger(__name__)

def test_search_term(driver, site_configs):
    LOGGER.info("Search term")
    search_page = SearchPage(driver, site_configs)
    term = "old man"
    # TODO: do not use rdriver
    search_page.search_term(term)
    assert_that(search_page.driver.title).is_equal_to(f"{term} - search | Open Library")