# Pytest GUI selenium testing 

Testing GUI automation project. Sites:

   - [https://opensource-demo.orangehrmlive.com/](https://opensource-demo.orangehrmlive.com/)
   - [https://www.bloghogwarts.com/](https://www.bloghogwarts.com/)

## Setup

Requires: python3, python virtualenv, make.

1. At `/main/core/resources/` create a `config.json` file based on `config_sample.json` do the same for `environment_sample.json`.

2. Install dependencies: `make setup`

3. Static code anyalisis: `make static_code_analysis`

4. Run tests: `make test`.